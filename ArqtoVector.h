#ifndef ARQTOVECTOR_H_INCLUDED
#define ARQTOVECTOR_H_INCLUDED

void arqtovector(vector<string> &fnc, vector<string> &producoes, vector<string> &variaveis,vector<string> &terminais,string alfabeto[],vector<string> &novasVariaveis ){
        string aux;
        aux = fnc[1];
        istringstream ss(aux);
        while (!ss.eof()){
          string x = "";
          getline( ss, x, ',' );
          x.erase(x.begin(),find_if(x.begin(), x.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
          variaveis.push_back(x);
        }
        aux = fnc[3];
        istringstream ss1(aux);
        while (!ss1.eof()){
          string x = "";
          getline( ss1, x, ',' );
          x.erase(x.begin(),find_if(x.begin(), x.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
          terminais.push_back(x);
        }
        for(int i = 5; i < fnc.size(); i++){
            string x = "";
            x = fnc[i];
            if(x != "Inicial:"){
                producoes.push_back(x);
            }else {
                break;
            }
        }
         for(int i = 0; i < terminais.size(); i++){
            novasVariaveis.push_back(alfabeto[i]);
        }
        for(int i = 0; i < terminais.size(); i++){
            string buf = "";
            buf += novasVariaveis[i];
            buf += " -> ";
            buf += terminais[i];
            string var = "";
            string ter = "";
            string prod = "";
            var = novasVariaveis[i];
            ter = terminais[i];
            for(int j = 0; j < producoes.size(); j++){
                prod = producoes[j];
                int cont = 0,achou = 0;
                for(int f = 0; f < prod.size(); f++){
                    if(prod[f] == '>'){
                        achou = 1;
                    }
                    if(achou == 1){
                        cont++;
                    }
                }
                if(cont-2 == 3){
                    for(int k = 0; k < prod.size(); k++){
                        if(prod[k] == ter[0]){
                            prod[k] = var[0];
                        }
                    }
                }
                producoes[j] = prod;
            }
            producoes.push_back(buf);
        }
}

#endif // ARQTOVECTOR_H_INCLUDED

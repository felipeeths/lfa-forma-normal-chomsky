#ifndef ARQUIVOS_HPP_INCLUDED
#define ARQUIVOS_HPP_INCLUDED

#include <fstream>

using namespace std;

void carregaArq(vector<string> &fnc){
    ifstream arq;
    string buff;
    arq.open("EntradaFNC.txt",ios::in);
    while(arq.good()){
        getline(arq,buff);
        fnc.push_back(buff);

    }
    arq.close();
}


void salvaArq(string variaveiss,string terminaiss,vector<string> &producoes,string efinal){
    ofstream arq;
    arq.open("SaidaFNC.txt",ios::out);
    arq << "Vari�veis:" << endl;
    arq << variaveiss << endl;
    arq << "Terminais:" << endl;
    arq << terminaiss << endl;
    arq << "Produ��es:" << endl;
    for(int i = 0; i < producoes.size(); i++){
        arq << producoes[i] << endl;
    }
    arq << "Inicial:" << endl;
    arq << efinal;
    arq.close();
}

#endif // ARQUIVOS_HPP_INCLUDED

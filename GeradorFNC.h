#ifndef GERADORFNC_H_INCLUDED
#define GERADORFNC_H_INCLUDED

void GerarFNC(vector<string> &fnc, vector<string> &producoes, vector<string> &variaveis,vector<string> &terminais,string alfabeto[],vector<string> &novasVariaveis,vector<string> &novasproducoes,vector<string> &novasVariaveisGeradas){
    string producao = "";
    string resultado;
    for(int i = 0; i < producoes.size(); i++){///pega as produ��es maiores que 2 terminais
        if (producoes[i].size() > 6){
            producao = producoes[i];
            resultado = producao[5];
            resultado += producao[6];
            resultado[0] = towupper(resultado[0]);
            novasproducoes.push_back(resultado);
         }
    }

    for(int i = 0; i < novasproducoes.size(); i++){///Gera novas variaves
        string nova = "";
        string auxn = "";
        char p[10] = {'1','2','3','4','5','6','7','8','9'};
        auxn = alfabeto[6];
        auxn += p[i];
        nova += auxn;
        novasVariaveisGeradas.push_back(auxn);
        nova += " -> ";
        nova += novasproducoes[i];
        producoes.push_back(nova);
    }
    int g = 0;
    int c = 0;
    for(int i = 0; i < producoes.size(); i++){
        string x = "";
        x = producoes[i];
        int vet;
        int cont = 0,achou = 0;
        for(int j = 0; j < x.size(); j++){
            if(x[j] == '>'){
                achou = 1;
            }
            if(achou == 1){
                cont++;
            }

        }
        int h = 0;
        if(cont-2 == 3){
            string buff = "";
            string pop = "";
            buff = producoes[i];
            string cop = "";
            cop = novasVariaveis[g];
            for(h = 0; h < buff[h]; h++){
                pop = terminais[g];
                if(buff[h] == pop[0]){
                    buff[h] = cop[0];
                }


            }
            producoes[i] = buff;
            g++;
            int b = 0;
            string top = "";
            string rop = "";
            rop[0] = buff[5];
            rop[1] = buff[6];
            top = novasVariaveisGeradas[c];
            for(int h = 0; h < buff[h]; h++ ){
                if(buff[h] == rop[b]){
                    buff[h] = top[b];
                    b++;
                }
            }
            producoes[i] = buff;
            c++;
        }
   }

}

#endif // GERADORFNC_H_INCLUDED

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include "Arquivos.h"
#include "ArqtoVector.h"
#include "GeradorFNC.h"

/* Nomes : Felipe ferreira dos santos
           Gustavo Berlanda
   Tema: dada uma linguagem simplificada por  arquivo EntradaFNC.txt, transformar
   em uma gramatica na forma normal de Chomsky criada no arquivo SaidaFNC.txt*/

using namespace std;
int main()
{
    vector<string> fnc;
    vector<string> variaveis;
    vector<string> novasVariaveis;
    vector<string> terminais;
    vector<string> producoes;
    vector<string> novasproducoes;
    vector<string> novasVariaveisGeradas;
    string alfabeto[] = {"A","B","C","D","E","F","G"};

    carregaArq(fnc); ///carrega todo arquivo pro vector fnc

    ///++++++++++++++++++++++++++++++
    arqtovector(fnc,producoes,variaveis,terminais,alfabeto,novasVariaveis); /// reparte o fnc em outros vectors
                ///salvar o fnc gerado para arquivo de saida
    GerarFNC(fnc,producoes,variaveis,terminais,alfabeto,novasVariaveis,novasproducoes,novasVariaveisGeradas);///criacao do fnc
    string variaveiss;
    for(int i = 0; i < variaveis.size(); i++){
            variaveiss += variaveis[i];
            variaveiss += ", ";
    }
    for(int i = 0; i < novasVariaveis.size(); i++){
        if(i < novasVariaveis.size()){
            variaveiss += novasVariaveis[i];
            if(novasVariaveisGeradas.size() > 0)
            variaveiss += ", ";
        }
    }
    for(int i = 0; i < novasVariaveisGeradas.size(); i++){
        if(i < novasVariaveis.size()-1){
            variaveiss += novasVariaveisGeradas[i];
            variaveiss += ", ";
        }else {
            variaveiss += novasVariaveisGeradas[i];

        }
    }

    string terminaiss;
    terminaiss = fnc[3];
    string efinal;
    for(int i = 0; i < fnc.size(); i++){
        if(fnc[i] == "Inicial:"){
            efinal = fnc[i+1];
        }
    }
    salvaArq(variaveiss,terminaiss,producoes,efinal);//salvar o fnc gerado para arquivo de saida
    cout << " Arquivo Gerado!!" << endl;
    cout << " SaidaFNC.txt" << endl;



    return 0;
}


